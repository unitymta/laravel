<html>
<head>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
    @include('layouts.header')

    <div id="content">
        <h1>unityMTA</h1>

        @yield('NoiDung')
        @yield('test')
    </div>
</body>
</html>
