<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('sang', function () {
    return 'Welcome to sang page!';
});
Route::get('sang/page', function () {
    echo '<h1>unityMTA</h1>';
});

Route::get('hoten/{ten}', function ($ten) {
    echo 'Ho ten la: ' . $ten;
});
Route::get('laravel/{ngay}', function ($ngay) {
    echo 'ngay: ' . $ngay;
})->where(['ngay' => '[0-9-]+']);
Route::get('laravel/{ten}', function ($ten) {
    echo 'ten: ' . $ten;
})->where(['ten' => '[a-zA-Z]+']);

// DInh danh
Route::get('dinhdanh', ['as' => 'myroute', function () {
    echo 'Day la dinh danh route';
}]);
Route::get('goiten', function () {
    return redirect()->route('myroute');
});
Route::get('dinhdanh2', function () {
    echo 'dinh danh 22';
})->name('myroute2');
Route::get('goiten2', function () {
    return redirect()->route('myroute2');
});

//Group
Route::group(['prefix' => 'mygroup'], function () {
    Route::get('user1', function () {
        return 'user 1';
    });
    Route::get('user2', function () {
        return 'user2';
    });
    Route::get('user3', function () {
        echo 'user 3';
    });
});

// Goi controller
Route::get('controller', 'MyController@XinChao');
Route::get('thamso/{ten}', 'MyController@KhoaHoc')->where(['ten' => '[a-zA-Z]+']);

// URL
Route::get('myrequest', 'MyController@GetUrl');

// GUi nhan du lieu
Route::get('getform', function () {
    return view('postForm');
});
Route::post('postform', ['as' => 'postform', 'uses' => 'MyController@postForm']);

//Cookie
Route::get('setCookie', 'MyController@setCookie');
Route::get('getCookie', 'MyController@getCookie');

// Upload file
Route::get('uploadFile', function () {
    return view('postFile');
});
Route::post('postFile', ['as' => 'postFile', 'uses' => 'MyController@postFile']);

// Json
Route::get('getJson', 'MyController@getJson');

// Truyen tham so tren view
Route::get('time/{t}', 'MyController@Time');
View::share('share_param', 'print parameter');


// balde template
Route::get("blade", function () {
    return view("pages.laravel");
});
Route::get('test', 'MyController@Test');


//query builder

Route::get('qb/get', function () {
    $data = DB::table('users')->get();
//    dd($data);

    foreach ($data as $row) {
        foreach ($row as $key => $value) {
            echo $key . ":" . $value . "<br>";
        }
        echo "<hr>";
    }
});

Route::get('qb/where', function () {
    $data = DB::table('users')->where('id', '!=', 3)->get();
    foreach ($data as $row) {
        foreach ($row as $key => $value) {
            echo $key . ":" . $value . "<br>";
        }
        echo "<hr>";
    }
});

Route::get('qb/select', function () {
    $data = DB::table('users')->select('id', 'name', 'email')->where('id', '!=', 4)->get();

    foreach ($data as $row) {
        foreach ($row as $key => $value) {
            echo $key . ":" . $value . "<br>";
        }
        echo "<hr>";
    }
});

Route::get('qb/raw', function () {
    $data = DB::table('users')->select(DB::raw('id, name as hoten, email'))->where('id', '=', 4)->get();

    foreach ($data as $row) {
        foreach ($row as $key => $value) {
            echo $key . ":" . $value . "<br>";
        }
        echo "<hr>";
    }
});

// model
Route::get('model/save', function(){
   $user = new App\User();
   $user->name = 'Sang';
   $user->email = 'san213g@gmail.com';
   $user->password = 'Mat khau';

   $user->save();
   echo 'da save';
});








