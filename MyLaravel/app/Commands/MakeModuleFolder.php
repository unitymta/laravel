<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;

class MakeModuleFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:make {name} {--db=true} {--helper=true} {--model=true} {--resource=true}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make module folders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $moduleName = $this->argument('name');
        $struct     = "
        |- Modules
            |- $moduleName";
        $moduleRootPath = app_path() . '/Modules/' . $moduleName;
        if (File::isDirectory($moduleRootPath) && $this->confirm('Folder exists!Do you want to overwrite?')) {
            File::deleteDirectory($moduleRootPath);
        }
        $listFolders = [
            'Database/Migrations'              => [
                'allow'  => $this->option('db') == "true",
                'struct' => "
                |- Database
                    |- Migrations",
            ],
            'Database/Seeders'                 => [
                'allow'  => $this->option('db') == "true",
                'struct' => "
                    |- Seeders",
            ],
            'Helpers'                          => [
                'allow'  => $this->option('helper') == "true",
                'struct' => "
                |- Helpers",
            ],
            'Http/Controllers'                 => [
                'allow'  => true,
                'struct' => "
                |- Http
                    |- Controllers",
            ],
            "Http/Controllers/{$moduleName}Controller.php" => [
                'allow'   => true,
                'content' => preg_replace('/#ALIAS#/', strtolower($moduleName), preg_replace('/#MODULE#/', $moduleName, file_get_contents(__DIR__ . '/controller.txt'))),
                'struct'  => "
                        |- {$moduleName}Controller.php",
            ],
            'Http/Middlewares'                 => [
                'allow'  => true,
                'struct' => "
                    |- Middlewares",
            ],
            'Http/routes.php'                  => [
                'allow'   => true,
                'content' => "<?php
Route::get('/', '{$moduleName}Controller@index');
                ",
                'struct' => "
                    |- routes.php",
            ],
            'Models'                           => [
                'allow'  => $this->option('model') == "true",
                'struct' => "
                |- Models",
            ],
            'Resources/views'                  => [
                'allow'  => $this->option('resource') == "true",
                'struct' => "
                |- Resources
                    |- views",
            ],
            'Resources/views/index.blade.php'                  => [
                'allow'   => $this->option('resource') == "true",
                'content' => "<h4>$moduleName index view.</h4>",
                'struct'  => "
                        |- index.blade.php",
            ],
            "{$moduleName}ServiceProvider.php" => [
                'allow'   => true,
                'content' => preg_replace('/#MODULE#/', $moduleName, file_get_contents(__DIR__ . '/provider.txt')),
                'struct'  => "
                {$moduleName}ServiceProvider.php
                ",
            ],
        ];
        foreach ($listFolders as $path => $option) {
            if ($option['allow']) {
                if (isset($option['content']) && $option['content']) {
                    File::put("$moduleRootPath/$path", $option['content']);
                } else {
                    File::makeDirectory("$moduleRootPath/$path", 0755, true, true);
                }
                $struct .= $option['struct'];
            }
        }
        $this->line('Created module successfully.');
        $this->line("$moduleName's struct:");
        $this->info($struct);
        $this->line("Please add {$moduleName}ServiceProvider.php to \"providers\" in config/app.php");
    }
}
