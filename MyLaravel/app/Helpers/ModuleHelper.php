<?php

namespace App\Helpers;

use File;
use Illuminate\Support\Facades\Route;

class ModuleHelper
{
    public static $moduleName;

    const ROOT_NAMESPACE       = 'App\\Modules\\';
    const CONTROLLER_NAMESPACE = '\\Http\\Controllers';
    const VIEW_DIR             = '/Resources/views';
    const MIGRATION_DIR        = '/Database/Migrations';
    const SEEDER_DIR           = '/Database/Seeders';

    /**
     * set module name
     * @param string $moduleName
     */
    public static function setModuleName($moduleName)
    {
        self::$moduleName = $moduleName;
    }

    /**
     * load all routes
     * @return mixed
     */
    public static function loadAllRoutes()
    {
        $listModules = File::directories(app_path() . '/Modules/');
        foreach ($listModules as $modulePath) {
            $moduleName = preg_replace('/^(.*)?\//', '', $modulePath);
            self::loadRoutes($moduleName);
        }
    }

    /**
     * load module routes
     * @return mixed
     */
    public static function loadRoutes()
    {
        $routeFile = app_path() . '/Modules/' . self::$moduleName . '/Http/routes.php';
        if (file_exists($routeFile)) {
            Route::middleware('web')
                ->prefix(self::getViewAlias())
                ->name(self::getViewAlias() . '.')
                ->namespace(self::getControllersNameSpace())
                ->group($routeFile);
        }
    }

    /**
     * get view dir
     * @return string
     */
    public static function getViewsDir()
    {
        return app_path() . '/Modules/' . self::$moduleName . self::VIEW_DIR;
    }

    /**
     * get controller dir
     * @return string
     */
    public static function getControllersNameSpace()
    {
        return self::ROOT_NAMESPACE . self::$moduleName . self::CONTROLLER_NAMESPACE;
    }

    /**
     * get view alias
     * @return string
     */
    public static function getViewAlias()
    {
        return strtolower(self::$moduleName);
    }

    /**
     * get migrations dir
     * @return string
     */
    public static function getMigrationsDir()
    {
        return app_path() . '/Modules/' . self::$moduleName . self::MIGRATION_DIR;
    }
}
