<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DateTime;

class MyController extends Controller
{
    public function XinChao()
    {
        echo 'Xin chao, controller';
    }

    public function KhoaHoc($ten)
    {
        echo 'ten: ' . $ten;
    }

    public function GetUrl(Request $request)
    {
        if ($request->isMethod('get')) {
            echo 'Phuong thuc GET<br>';
        } else {
            echo 'Khong phai phunog thuc get<br>';
        }

        if ($request->is('*re*')) {
            echo 'Co my<br>';
        } else {
            echo 'Khong co my<br>';
        }
    }

    public function postForm(Request $request)
    {
        echo $request->input('HoTen') . '<br>';
        if ($request->has('Tuoi')) {
            echo ' co Tuoi';
        } else {
            echo ' khong tuoi';
        }
    }

    public function setCookie()
    {
        $response = new Response;
        $response->withCookie('hoten', 'unityMTA', 0.1);
        return $response;
    }

    public function getCookie(Request $request)
    {
        return $request->cookie('hoten');
    }

    public function postFile(Request $request)
    {
        if ($request->hasFile('myFile') && ($request->hasFile('myFile2'))) {
            $date = new DateTime();
            $dateFormat = date_format($date, 'Ymd-H:i:s');
            $file = $request->file('myFile');
            $file2 = $request->file('myFile2');

            $nameFile = 'demo-' . $dateFormat . '.' . $file->clientExtension();
            $nameFile2 = 'demo2-' . $dateFormat . '.' . $file2->clientExtension();
            $file->move('img', $nameFile);
            $file2->move('img', $nameFile2);
        } else {
            echo 'Error upload file';
        }
    }

    public function getJson()
    {
        $array = ['Laravel' => 'content Laravel', 'unityMTA', 'sang'];
        return response()->json($array);
    }

    public function Time($t)
    {
        return view('myView', ['time' => $t]);
    }

    public function Test()
    {
        $t = 'sang test  test';
//        $t = "";
        return view('pages.test', ['ttt' => $t]);
    }
}



































